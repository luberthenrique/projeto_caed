using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace projeto_caed.Api.Models
{
    public class Itens
    {
        public Itens()
        {
            //this.Itens_Itens_Chave = new List<Itens_Itens_Chave>();
            //this.Chave = new List<Itens_Chave>();
        }
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("item")]
        public string Item { get; set; }

        [JsonProperty("referencia")]
        public string Referencia { get; set; }

        [JsonProperty("sequencial")]
        public string Sequencial { get; set; }

        [JsonProperty("solicitacao")]
        public string Solicitacao { get; set; }

        [JsonProperty("situacao")]
        public string Situacao { get; set; }

        [JsonProperty("ordem")]
        public long Ordem { get; set; }

        [JsonProperty("chave")]
        public List<Chaves> Chaves { get; set; }


        //public List<Chave> Chave
        //{
        //    get
        //    {
        //        //if (Itens_Chave != null && Itens_Chave.Any())
        //        //{
        //        //    return Itens_Chave.Select(c => c.Chave).ToList();
        //        //}
        //        //else if (chave != null && chave.Any())
        //        //{
        //        //    return chave;
        //        //}
        //        return new List<Chave>();
        //    }
        //    set
        //    {
        //        var x = 1;
        //        //this.chave = value;
        //    }
        //}
        [JsonIgnore]
        public virtual ICollection<Itens_Chaves> Itens_Chaves { get; set; }

        [JsonIgnore]
        public virtual ICollection<Correcoes> Correcoes { get; set; }
    }

}