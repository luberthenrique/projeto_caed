using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace projeto_caed.Api.Models
{
    public class Itens_Finalizados
    {
        public Itens_Finalizados()
        {
            //this.Itens_Itens_Chave = new List<Itens_Itens_Chave>();
            //this.Chave = new List<Itens_Chave>();
        }
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("item")]
        public string Item { get; set; }

        [JsonProperty("referencia")]
        public string Referencia { get; set; }

        [JsonProperty("sequencial")]
        public string Sequencial { get; set; }

        [JsonProperty("solicitacao")]
        public string Solicitacao { get; set; }

        [JsonProperty("situacao")]
        public string Situacao { get; set; }

        [JsonIgnore]
        public long Ordem { get; set; }

        [JsonProperty("chave")]        
        public virtual ICollection<Itens_Chaves> Itens_Chaves { get; set; }

    }

}