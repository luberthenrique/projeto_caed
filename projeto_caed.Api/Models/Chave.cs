using Newtonsoft.Json;
using System.Collections.Generic;

namespace projeto_caed.Api.Models
{
    public class Chaves
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("titulo")]
        public string Titulo { get; set; }

        [JsonProperty("opcoes")]
        public virtual List<Opcoes> Opcoes { get; set; }
        [JsonIgnore]
        public virtual ICollection<Itens_Chaves> Itens_Chaves { get; set; }
    }
}