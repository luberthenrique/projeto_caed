﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace projeto_caed.Api.Models
{
    public class Itens_Chaves
    {
        [JsonIgnore]
        public long ItensId { get; set; }
        [JsonProperty("id")]
        public long ChavesId { get; set; }
        [JsonProperty("valor")]
        public string Valor { get; set; }

        [JsonIgnore]
        public Itens Itens { get; set; }
        [JsonIgnore]
        public Chaves Chaves { get; set; }
    }
}
