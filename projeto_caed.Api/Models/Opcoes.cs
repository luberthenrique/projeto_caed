using Newtonsoft.Json;
using System.Collections.Generic;

namespace projeto_caed.Api.Models
{
    public class Opcoes
    {
        [JsonIgnore]
        public int Id { get; set; }
        [JsonIgnore]
        public long ChavesId { get; set; }
        [JsonProperty("valor")]
        public string Valor { get; set; }

        [JsonProperty("descricao")]
        public string Descricao { get; set; }

        [JsonIgnore]
        public virtual Chaves Chaves { get; set; }
    }
}