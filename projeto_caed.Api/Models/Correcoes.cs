using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;

namespace projeto_caed.Api.Models
{
    public class Correcoes
    {
        [JsonIgnore]
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonIgnore]
        [JsonProperty("idItens")]
        public long ItensId { get; set; }
        [DefaultValue("")]
        [JsonProperty("data", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public object Itens { get; set; }
        [JsonProperty("situacao")]
        public string Situacao { get; set; }
        [JsonProperty("tipo", NullValueHandling = NullValueHandling.Ignore)]
        public string Tipo { get; set; }
        [JsonProperty("descriçao", NullValueHandling = NullValueHandling.Ignore)]
        public string Descricao { get; set; }

        [JsonProperty("chave", NullValueHandling = NullValueHandling.Ignore)]
        public virtual List<Itens_Chaves> Itens_Chaves { get;set; }
    }
}