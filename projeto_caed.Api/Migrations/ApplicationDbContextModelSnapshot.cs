﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Projeto_Caed.Api.Data;

namespace projeto_caed.Api.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity("projeto_caed.Api.Models.Chaves", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Titulo");

                    b.HasKey("Id");

                    b.ToTable("Chaves");
                });

            modelBuilder.Entity("projeto_caed.Api.Models.Itens", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Item");

                    b.Property<long>("Ordem");

                    b.Property<string>("Referencia");

                    b.Property<string>("Sequencial");

                    b.Property<string>("Situacao");

                    b.Property<string>("Solicitacao");

                    b.HasKey("Id");

                    b.ToTable("Itens");
                });

            modelBuilder.Entity("projeto_caed.Api.Models.Itens_Chaves", b =>
                {
                    b.Property<long>("ChavesId");

                    b.Property<long>("ItensId");

                    b.Property<string>("Valor");

                    b.HasKey("ChavesId", "ItensId");

                    b.HasIndex("ItensId");

                    b.ToTable("Itens_Chaves");
                });

            modelBuilder.Entity("projeto_caed.Api.Models.Opcoes", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("ChavesId");

                    b.Property<string>("Descricao");

                    b.Property<string>("Valor");

                    b.HasKey("Id");

                    b.HasIndex("ChavesId");

                    b.ToTable("Opcoes");
                });

            modelBuilder.Entity("projeto_caed.Api.Models.Itens_Chaves", b =>
                {
                    b.HasOne("projeto_caed.Api.Models.Chaves", "Chaves")
                        .WithMany("Itens_Chaves")
                        .HasForeignKey("ChavesId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("projeto_caed.Api.Models.Itens", "Itens")
                        .WithMany("Itens_Chaves")
                        .HasForeignKey("ItensId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("projeto_caed.Api.Models.Opcoes", b =>
                {
                    b.HasOne("projeto_caed.Api.Models.Chaves", "Chaves")
                        .WithMany("Opcoes")
                        .HasForeignKey("ChavesId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
