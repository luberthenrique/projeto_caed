﻿using Microsoft.EntityFrameworkCore;
using projeto_caed.Api.Models;

namespace Projeto_Caed.Api.Data
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder){
            base.OnModelCreating(modelBuilder);

            //Relacionamento classe Itens_Chave_Opcao x Itens_Chave
            modelBuilder.Entity<Itens_Chaves>()
              .HasKey(s => new { s.ChavesId , s.ItensId });

            //Definindo objeto somente de acesso a dados
            modelBuilder.Entity<Itens>()
              .Ignore(s => s.Chaves);

            //Classe de acesso a dados, não salvar no banco
            modelBuilder.Ignore<Correcoes>();           

        }

        public DbSet<projeto_caed.Api.Models.Itens> Itens { get; set; }

        public DbSet<projeto_caed.Api.Models.Chaves> Chaves { get; set; }
        public DbSet<projeto_caed.Api.Models.Opcoes> Opcoes { get; set; }

        public DbSet<projeto_caed.Api.Models.Itens_Chaves> Itens_Chaves { get; set; }
    }
}