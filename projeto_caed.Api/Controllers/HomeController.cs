using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using projeto_caed.Api.Models;
using Projeto_Caed.Api.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace projeto_caed.Api.Controllers
{
    public class HomeController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Chave
        [HttpGet]
        public ActionResult Index()
        {
            return Redirect("/swagger");
        }        
    }
}
