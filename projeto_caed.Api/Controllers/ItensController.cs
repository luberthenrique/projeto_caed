﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Projeto_Caed.Api.Data;
using projeto_caed.Api.Models;

namespace projeto_caed.Api.Controllers
{
    [Route("itens")]
    [ApiController]
    public class ItensController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ItensController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Itens
        [HttpGet]
        public async Task<ActionResult<object>> GetItens()
        {
            var itens = await _context.Itens.Include(c=> c.Itens_Chaves).ThenInclude(c=> c.Chaves).ThenInclude(c=> c.Opcoes).ToListAsync();

            if (itens.Any())
            {
                foreach (var item in itens)
                {
                    if (item.Itens_Chaves.Any())
                    {
                        //Recuperando chaves relacionadas a tabela N:N de chaves e inserindo no objeto itens para visualizacao
                        item.Chaves = await _context.Itens_Chaves.Include(c => c.Chaves).ThenInclude(c => c.Opcoes).Where(c => c.ItensId == item.Id).Select(c => c.Chaves).ToListAsync();
                    }
                }

                return itens;
            }
            return new Retorno { Situacao = "ERRO", Tipo = "SEM_ITENS", Descricao = "Não existe itens cadastrados" };
            
        }

        // GET: Itens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Itens>> GetItens(long id)
        {
            var itens = await _context.Itens.FindAsync(id);

            if (itens == null)
            {
                return NotFound();
            }

            return itens;
        }

        // PUT: Itens/5
        [HttpPut("{id}")]
        public async Task<ActionResult<object>> PutItens(long id, Itens itens)
        {
            if (id != itens.Id)
            {
                return BadRequest();
            }

            _context.Entry(itens).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                return new Retorno { Situacao = "SUCESSO", Descricao = "Item atualizado com sucesso" };
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItensExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            //return NoContent();
        }

        // POST: Itens
        [HttpPost]
        public async Task<ActionResult<Retorno>> PostItens(Itens itens)
        {
            if (_context.Itens.Any(c => c.Id == itens.Id))
            {
                return new Retorno { Situacao = "Erro", Tipo = "ITEM_CADASTRADO", Descricao = "Item já cadastrado" };
            }

            //Adicionando chaves relacionadas aos itens
            if (itens != null && itens.Chaves.Any())
            {
                //Salvar chaves no banco de dados caso as mesmas não existão
                var chaves = itens.Chaves.Where(c => !_context.Chaves.Select(d => d.Id).Contains(c.Id));
                _context.Chaves.AddRange(chaves);
                await _context.SaveChangesAsync();

                //Recarrecando chaves que estão no banco de dados e que estão relacionadas com o objeto a ser salvo(itens)
                chaves = itens.Chaves.Where(c => _context.Chaves.Select(d => d.Id).Contains(c.Id));

                //Salvando chaves relacionadas com o objeto a ser salvo em uma classe N:N
                var itens_chaves = chaves.Select(c => new Itens_Chaves { ChavesId = c.Id, ItensId = itens.Id }).ToList();
                _context.Itens_Chaves.AddRange(itens_chaves);
                itens.Itens_Chaves = itens_chaves;
            }

            _context.Itens.Add(itens);
            
            await _context.SaveChangesAsync();

            return new Retorno { Situacao = "SUCESSO", Descricao = "Item salvo com sucesso" };
        }

        // DELETE: Itens/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Retorno>> DeleteItens(long id)
        {
            try
            {
                var itens = await _context.Itens.FindAsync(id);
                if (itens == null)
                {
                    return NotFound();
                }

                //Remover itens relacionados na tabela N:N

                _context.Itens.Remove(itens);
                //var x = await _context.Itens_Itens_Chave.ToListAsync();
               // _context.Itens_Itens_Chave.RemoveRange(await _context.Itens_Itens_Chave.Where(c => c.IdItens == id).ToListAsync());
                await _context.SaveChangesAsync();

                return new Retorno { Situacao = "SUCESSO", Descricao = "Item excluído com sucesso" };
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        private bool ItensExists(long id)
        {
            return _context.Itens.Any(e => e.Id == id);
        }
    }
}
