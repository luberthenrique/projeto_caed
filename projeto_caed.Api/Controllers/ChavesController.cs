﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using projeto_caed.Api.Models;
using Projeto_Caed.Api.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace projeto_caed.Api.Controllers
{
    [Route("chaves")]
    [ApiController]
    public class ChavesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ChavesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Chave
        [HttpGet]
        public async Task<ActionResult<object>> GetChaves()
        {
            var chaves = await _context.Chaves.Include(c => c.Opcoes).ToListAsync();
            if (chaves.Any())
            {
                return chaves;
            }
            return new Retorno { Situacao = "ERRO", Tipo = "SEM_CHAVES", Descricao = "Não existem chaves cadastradas" };
        }

        // GET: Chave/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Chaves>> GetChaves(long id)
        {
            var chaves = await _context.Chaves.FindAsync(id);
            
            if (chaves == null)
            {                

                return NotFound();
            }
            else
            {
                chaves.Opcoes = await _context.Opcoes.Where(c => c.ChavesId == chaves.Id).ToListAsync();

            }

            return chaves;
        }

        // PUT: Chave/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChaves(long id, Chaves chaves)
        {
            if (id != chaves.Id)
            {
                return BadRequest();
            }

            _context.Entry(chaves).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChavesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: Chave
        [HttpPost]
        public async Task<ActionResult<Retorno>> PostChaves(Chaves chaves)
        {
            _context.Chaves.Add(chaves);
            await _context.SaveChangesAsync();

            return new Retorno { Situacao = "SUCESSO", Descricao = "Chave salva com sucesso" };
        }

        // DELETE: Chave/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Retorno>> DeleteChaves(long id)
        {
            var chaves = await _context.Chaves.FindAsync(id);
            if (chaves == null)
            {
                return NotFound();
            }

            _context.Chaves.Remove(chaves);
            await _context.SaveChangesAsync();

            return new Retorno { Situacao = "SUCESSO", Descricao = "Chave excluída com sucesso" };
        }

        private bool ChavesExists(long id)
        {
            return _context.Chaves.Any(e => e.Id == id);
        }
    }
}
