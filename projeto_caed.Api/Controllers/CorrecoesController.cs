using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Projeto_Caed.Api.Data;
using projeto_caed.Api.Models;

namespace Projeto_Caed.Api.Controllers
{
    [Route("correcoes")]
    [ApiController]
    public class CorrecoesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CorrecoesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Correcoes

        [HttpGet]
        public async Task<ActionResult<Correcoes>> GetCorrecoes()
        {

            //Listando todas as correções que já foram realizadas
            var itens = await _context.Itens
                .Include(c => c.Itens_Chaves)
                    .ThenInclude(c => c.Chaves)
                        .ThenInclude(c => c.Opcoes)
               .Where(c => c.Situacao == "DISPONIVEL" || c.Situacao == "RESERVADA").ToListAsync();

            foreach (var item in itens)
            {
                item.Chaves = item.Itens_Chaves.Select(c => c.Chaves).ToList();
            }

            if (itens.Any())
            {
                return new Correcoes { Itens = itens, Situacao = "SUCESSO" };
            }

            return new Correcoes { Situacao = "ERRO", Tipo = "SEM_CORRECAO", Descricao = "Não existem mais correções disponíveis" };

        }

        [HttpGet]
        [Route("finalizadas")]
        public async Task<ActionResult<Correcoes>> GetCorrecoes_Finalizadas()
        {

            //Listando todas as correções que já foram realizadas
            var itens = await _context.Itens
                .Include(c => c.Itens_Chaves)
                    .ThenInclude(c => c.Chaves)
                        .ThenInclude(c => c.Opcoes)
                .Include(c=> c.Itens_Chaves)
               .Where(c => c.Situacao != "DISPONIVEL").Select(c=>
                new Itens_Finalizados { Item = c.Item, Referencia = c.Referencia, Sequencial = c.Sequencial, Solicitacao = c.Solicitacao,Situacao = c.Situacao, Itens_Chaves = c.Itens_Chaves}
               ).ToListAsync();

            if (itens.Any())
            {
                return new Correcoes { Itens = itens, Situacao = "SUCESSO" };
            }

            return new Correcoes { Situacao = "ERRO", Tipo = "SEM_CORRECAO", Descricao = "Não existe correções finalizadas" };

        }

        // GET: Correcoes/Proxima
        [HttpGet]
        [Route("proxima")]
        public async Task<ActionResult<Correcoes>> GetProxima(bool reservada = false)
        {
            var itens = await _context.Itens
                .Include(c=> c.Itens_Chaves)
                    .ThenInclude(c=> c.Chaves)
                        .ThenInclude(c=> c.Opcoes)
                .Where(c => c.Situacao ==  (reservada == true ? "RESERVADA" : "DISPONIVEL") || c.Situacao == (reservada == true ? "RESERVADA" : "RESERVADA"))
                .OrderBy(c=> c.Situacao)
                .ThenBy(c=> c.Ordem)
                .FirstOrDefaultAsync();            

            if (itens != null)
            {
                itens.Chaves = itens.Itens_Chaves.Select(c => c.Chaves).ToList();
                return new Correcoes { Itens = itens, Situacao = "SUCESSO" };
            }

            return new Correcoes { Situacao = "ERRO", Tipo = "SEM_CORRECAO", Descricao = "Não existem mais correções disponíveis" };
        }

        [HttpGet]
        [Route("reservadas")]
        public async Task<ActionResult<Correcoes>> GetCorrecoes_Reservadas()
        {
            var itens = await _context.Itens
                .Include(c => c.Itens_Chaves)
                    .ThenInclude(c => c.Chaves)
                        .ThenInclude(c => c.Opcoes)
                .Where(c => c.Situacao == "RESERVADA")
                .OrderBy(c => c.Ordem)
                .ToListAsync();

            if (itens.Any())
            {
                foreach (var item in itens)
                {
                    item.Chaves = item.Itens_Chaves.Select(c => c.Chaves).ToList();
                }
                return new Correcoes { Situacao = "SUCESSO", Itens = itens };
            }

            return new Correcoes { Situacao = "ERRO", Tipo = "SEM_CORRECAO", Descricao = "Não existem correções reservadas" };
        }

        // POST: Correcao/5
        [HttpPost("{idCorrecao}")]
        public async Task<ActionResult<Correcoes>> PostCorrecoes(long idCorrecao, Correcoes correcoes)
        {
            Itens itens = await _context.Itens.Include(c => c.Itens_Chaves).ThenInclude(c => c.Chaves).ThenInclude(c => c.Opcoes).FirstOrDefaultAsync(c => c.Id == idCorrecao);

            try
            {
                if (itens == null)
                {
                    return NotFound();
                }

                //Verifica se a ordem do próximo itens de correção está correto. Em caso de correções reservadas, deixar corrigir sem precisar seguir a ordem
                if ((await _context.Itens.Where(c => c.Situacao == "DISPONIVEL").AnyAsync() && await _context.Itens.Where(c => c.Situacao == "DISPONIVEL").MinAsync(c => c.Ordem) < itens.Ordem))
                {
                    return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "ITEM_INVALIDO", Descricao = "Item inválido para correção" };
                }

                if (itens.Situacao == "CORRIGIDA")
                {
                    return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "ITEM_CORRIGIDO", Descricao = "Item já corrigido" };
                }
                else if (!correcoes.Itens_Chaves.Any())
                {
                    return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "CHAVE_NAO_INFORMADA", Descricao = "Nenhuma chave de correção foi imformada" };
                }

                //Verifica se existem os Itens de correcao e os valores de cada chave de correcao passada
                foreach (var item in correcoes.Itens_Chaves)
                {
                    if (!itens.Itens_Chaves.Select(c => c.Chaves.Id).Contains(item.ChavesId))
                    {
                        return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "CHAVE_INCORRETA", Descricao = "Chave de correção inexistente. O item não possui chave " + item.ChavesId };
                    }
                    if (!itens.Itens_Chaves.FirstOrDefault(c => c.ChavesId == item.ChavesId).Chaves.Opcoes.Select(c => c.Valor).Contains(item.Valor))
                    {
                        return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "CHAVE_INCORRETA", Descricao = "Chave de correção incorreta. Valor '" + item.Valor + "' não é válido para o item " + item.ChavesId };
                    }
                }

                //Caso não tenha apresentado algum dos erros acima, salvar correção
                itens.Situacao = "CORRIGIDA";

                foreach (var item in correcoes.Itens_Chaves)
                {
                    var aux = itens.Itens_Chaves.FirstOrDefault(c => c.ItensId == idCorrecao && c.ChavesId == item.ChavesId);
                    aux.Valor = item.Valor;

                    _context.Itens_Chaves.Update(aux);
                }

                correcoes.ItensId = itens.Id;
                correcoes.Itens = "";
                correcoes.Situacao = "SUCESSO";
                correcoes.Descricao = "Correção salva com sucesso";

                _context.Itens.Update(itens);

                await _context.SaveChangesAsync();

                return correcoes;
            }
            catch (Exception)
            {
                return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "ERRO_INTERNO", Descricao = "Um erro foi gerado ao salvar a correção, favor revisar os dados e tentar novamente" };
            }
        }

        // POST: Correcao/5
        [HttpPost("reservadas/{idCorrecao}")]
        public async Task<ActionResult<Correcoes>> PostCorrecoes_Reservada(long idCorrecao, Correcoes correcoes)
        {
            Itens itens = await _context.Itens.Include(c => c.Itens_Chaves).ThenInclude(c => c.Chaves).ThenInclude(c => c.Opcoes).FirstOrDefaultAsync(c => c.Id == idCorrecao);

            try
            {
                if (itens == null)
                {
                    return NotFound();
                }

                //Verifica se a ordem do próximo itens está correto.
                if (await _context.Itens.Where(c => c.Situacao == "DISPONIVEL").AnyAsync() && await _context.Itens.Where(c => c.Situacao == "DISPONIVEL").MinAsync(c => c.Ordem) < itens.Ordem)
                {
                    return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "ITEM_INVALIDO", Descricao = "iIem inválido para correção" };
                }

                if (itens.Situacao == "CORRIGIDA")
                {
                    return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "ITEM_CORRIGIDO", Descricao = "Item já corrigido" };
                }
                else if (!correcoes.Itens_Chaves.Any())
                {
                    return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "CHAVE_NAO_INFORMADA", Descricao = "Nenhuma chave de correção foi imformada" };
                }

                //Verifica se existem os Itens de correção e os valores de cada chaves de correção passada
                foreach (var item in correcoes.Itens_Chaves)
                {
                    if (!itens.Itens_Chaves.Select(c => c.Chaves.Id).Contains(item.ChavesId))
                    {
                        return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "CHAVE_INCORRETA", Descricao = "Chave de correção inexistente. O item não possui chaves " + item.ChavesId };
                    }
                    if (!itens.Itens_Chaves.FirstOrDefault(c => c.ChavesId == item.ChavesId).Chaves.Opcoes.Select(c => c.Valor).Contains(item.Valor))
                    {
                        return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "CHAVE_INCORRETA", Descricao = "Chave de correção incorreta. Valor '" + item.Valor + "' não é válido para o item " + item.ChavesId };
                    }
                }

                itens.Situacao = "RESERVADA";

                foreach (var item in correcoes.Itens_Chaves)
                {
                    var aux = itens.Itens_Chaves.FirstOrDefault(c => c.ItensId == idCorrecao && c.ChavesId == item.ChavesId);
                    aux.Valor = item.Valor;

                    _context.Itens_Chaves.Update(aux);
                }

                correcoes.ItensId = itens.Id;
                correcoes.Itens = "";
                correcoes.Situacao = "SUCESSO";
                correcoes.Descricao = "Correção reservada com sucesso";

                _context.Itens.Update(itens);

                await _context.SaveChangesAsync();

                return correcoes;
            }
            catch (Exception)
            {
                return new Correcoes { Situacao = "ERRO", Itens = "", Tipo = "ERRO_INTERNO", Descricao = "Um erro foi gerado ao salvar a correção, favor revisar os dados e tentar novamente" };

            }

        }

        private bool CorrecoesExists(int id)
        {
            return _context.Itens.Where(c=> c.Situacao != "DISPONIVEL").Any(e => e.Id == id);
        }
    }
}
