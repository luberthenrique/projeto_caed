﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using projeto_caed.Api.Controllers;
using projeto_caed.Api.Models;
using Projeto_Caed.Api.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace projeto_caed.Testes.API
{
    public class ChavesApiTest
    {
        public static IEnumerable<object[]> Data =>
        new List<object[]>
        {
            new object[]
            {
                new Chaves
                {
                    Id = 186,
                    Titulo = "Chave de correção 1",
                    Opcoes = new List<Opcoes>
                    {
                        new Opcoes
                        {
                            Valor = "0",
                            Descricao = "Certo"
                        },
                        new Opcoes
                        {
                            Valor = "1",
                            Descricao = "Errado"
                        }
                    }
                }
            },
            new object[]
            {
                new Chaves
                {
                    Id = 187,
                    Titulo = "Chave de correção 2",
                    Opcoes = new List<Opcoes>
                    {
                        new Opcoes
                        {
                            Valor = "0",
                            Descricao = "Lido"
                        },
                        new Opcoes
                        {
                            Valor = "1",
                            Descricao = "Não lido"
                        }
                    }
                }
            },
            new object[]
            {
                new Chaves
                {
                    Id = 188,
                    Titulo = "Chave de correção 3",
                    Opcoes = new List<Opcoes>
                    {
                        new Opcoes
                        {
                            Valor = "0",
                            Descricao = "0"
                        },
                        new Opcoes
                        {
                            Valor = "1",
                            Descricao = "1"
                        },
                        new Opcoes
                        {
                            Valor = "2",
                            Descricao = "2"
                        },
                        new Opcoes
                        {
                            Valor = "3",
                            Descricao = "3"
                        },
                        new Opcoes
                        {
                            Valor = "4",
                            Descricao = "4"
                        },
                        new Opcoes
                        {
                            Valor = "5",
                            Descricao = "5"
                        }
                    }
                }
            }
        };

        [Fact]
        public async Task Get()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                context.Database.EnsureCreated();
            }

            using (var context = new ApplicationDbContext(options))
            {
                var service = new ChavesController(context);

                var chave = new Chaves
                {
                    Id = 186,
                    Titulo = "Chave de correção 1",
                    Opcoes = new List<Opcoes>
                {
                    new Opcoes
                    {
                        Valor = "0",
                        Descricao = "Certo"
                    },
                    new Opcoes
                    {
                        Valor = "1",
                        Descricao = "Errado"
                    }
                }
                };

                await service.PostChaves(chave);
                var retorno = await service.GetChaves();

                Assert.IsType<List<Chaves>>(retorno.Value);
            }

            using (var context = new ApplicationDbContext(options))
            {
                Assert.Equal(1, await context.Chaves.CountAsync());
                Assert.Equal("Chave de correção 1", (await context.Chaves.SingleAsync()).Titulo);
            }
        }

        [Fact]
        public async Task Get_NenhumRetorno()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                context.Database.EnsureCreated();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var service = new ChavesController(context);

                var retorno = await service.GetChaves();

                Assert.IsType<Retorno>(retorno.Value);
                Assert.Equal("ERRO", ((Retorno)retorno.Value).Situacao);
                Assert.Equal("SEM_CHAVES", ((Retorno)retorno.Value).Tipo);
            }
            using (var context = new ApplicationDbContext(options))
            {
                Assert.Equal(0, await context.Itens.CountAsync());
            }      
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task Get_Id(Chaves chave)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;
                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ChavesController(context);

                    await service.PostChaves(chave);

                    var retorno = await service.GetChaves(chave.Id);

                    Assert.IsType<Chaves>(retorno.Value);

                    Assert.Equal(chave.Titulo, retorno.Value.Titulo);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task Post(Chaves chave)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite(connection)
                .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ChavesController(context);
                    var retorno = await service.PostChaves(chave);

                    Assert.IsType<Retorno>(retorno.Value);
                }
                using (var context = new ApplicationDbContext(options))
                {
                    Assert.Equal(1, await context.Chaves.CountAsync());
                    Assert.Equal(chave.Titulo, (await context.Chaves.SingleAsync()).Titulo);
                    Assert.Equal(chave.Opcoes.Count, await context.Opcoes.CountAsync(c => c.ChavesId == chave.Id));
                }

            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task Put(Chaves chave)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;
                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ChavesController(context);

                    chave.Titulo = "TESTE";

                    var retorno = await service.PostChaves(chave);

                    Assert.IsType<Retorno>(retorno.Value);
                    Assert.Equal("SUCESSO", retorno.Value.Situacao);
                }
                using (var context = new ApplicationDbContext(options))
                {
                    Assert.Equal("TESTE", (await context.Chaves.FindAsync(chave.Id)).Titulo);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task Delete(Chaves chave)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite(connection)
                .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new ApplicationDbContext(options))
                {                   
                    var service = new ChavesController(context);
                    var retorno = await service.PostChaves(chave);

                    retorno = await service.DeleteChaves(chave.Id);

                    Assert.IsType<Retorno>(retorno.Value);
                    Assert.Equal("SUCESSO", retorno.Value.Situacao);

                }
                using (var context = new ApplicationDbContext(options))
                {
                    Assert.Equal(0, await context.Chaves.CountAsync());
                }                

            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [InlineData(1111)]
        public async Task DeleteItens_Chave_ForceError(long id)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseSqlite(connection)
            .Options;

            using (var context = new ApplicationDbContext(options))
            {
                context.Database.EnsureCreated();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var service = new ChavesController(context);

                var retorno = await service.DeleteChaves(id);

                Assert.IsType<Microsoft.AspNetCore.Mvc.NotFoundResult>(retorno.Result);
            }
        }
    }
}
