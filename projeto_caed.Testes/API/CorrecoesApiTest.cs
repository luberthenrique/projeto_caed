using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using projeto_caed.Api.Controllers;
using projeto_caed.Api.Models;
using Projeto_Caed.Api.Controllers;
using Projeto_Caed.Api.Data;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace projeto_caed.Testes.API
{
    public class CorrecoesApiTest
    {

        public static IEnumerable<object[]> Itens =>
                new List<object[]>
                {
            new object[]
            {
                new Itens
                {
                    Id = 9859662,
                    Item = "D020006H6",
                    Referencia = "upload/correcao_9859662.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000885",
                    Situacao = "DISPONIVEL",
                    Ordem = 1,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        }}
                } },
            new object[]
            {
                new Itens
                {
                    Id = 9859663,
                    Item = "D320006C8",
                    Referencia = "upload/correcao_9859663.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000886",
                    Situacao = "DISPONIVEL",
                    Ordem = 2,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 187,
                            Titulo = "Chave de correção 2",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Lido"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Não lido"
                                }
                            }
                        }
                    }
                }
            },
            new object[]
            {
                new Itens
                {
                    Id = 9859664,
                    Item = "D6530006C7",
                    Referencia = "upload/correcao_9859664.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000886",
                    Situacao = "DISPONIVEL",
                    Ordem = 3,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 188,
                            Titulo = "Chave de correção 3",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "0"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "1"
                                },
                                new Opcoes
                                {
                                    Valor = "2",
                                    Descricao = "2"
                                },
                                new Opcoes
                                {
                                    Valor = "3",
                                    Descricao = "3"
                                },
                                new Opcoes
                                {
                                    Valor = "4",
                                    Descricao = "4"
                                },
                                new Opcoes
                                {
                                    Valor = "5",
                                    Descricao = "5"
                                }
                            }
                        }
                    }
                }
            }
                };

        [Theory]
        [MemberData(nameof(Itens))]
        public async Task Get_Proximas(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);
                }

                //Carregando próxima correção
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    var retorno = await service.GetProxima();

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("SUCESSO", retorno.Value.Situacao);
                    Assert.Equal(itens.Id, ((Itens)retorno.Value.Itens).Id);
                    Assert.Equal("DISPONIVEL", ((Itens)retorno.Value.Itens).Situacao);
                }

                //Forçando reserva da coreção do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes_Reservada(itens.Id, correcao);
                }

                //Verificando se correção foi reservada
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    var retorno = await service.GetProxima();

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("SUCESSO", retorno.Value.Situacao);
                    Assert.Equal(itens.Id, ((Itens)retorno.Value.Itens).Id);
                    Assert.Equal("RESERVADA", ((Itens)retorno.Value.Itens).Situacao);
                }

                //Forçando coreção do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes(itens.Id, correcao);
                }

                //Verificando se não existem mais itens a serem corrigidos
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    var retorno = await service.GetProxima();

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("ERRO", retorno.Value.Situacao);
                    Assert.Equal("SEM_CORRECAO", retorno.Value.Tipo);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Itens))]
        public async Task Get_Proximas_Reservada(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);
                }

                //Carregando próxima correção
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    var retorno = await service.GetProxima();

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("SUCESSO", retorno.Value.Situacao);
                    Assert.Equal(itens.Id, ((Itens)retorno.Value.Itens).Id);
                    Assert.Equal("DISPONIVEL", ((Itens)retorno.Value.Itens).Situacao);
                }

                //Forçando reserva da coreção do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes_Reservada(itens.Id, correcao);
                }

                //Carregando próximo item reservado
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    var retorno = await service.GetProxima(true);

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("SUCESSO", retorno.Value.Situacao);
                    Assert.Equal(itens.Id, ((Itens)retorno.Value.Itens).Id);
                    Assert.Equal("RESERVADA", ((Itens)retorno.Value.Itens).Situacao);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Fact]
        public async Task Get_Reservadas()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    var item = new Itens
                    {
                        Id = 9859662,
                        Item = "D020006H6",
                        Referencia = "upload/correcao_9859662.png",
                        Sequencial = "8300003130128",
                        Solicitacao = "2000000885",
                        Situacao = "DISPONIVEL",
                        Ordem = 1,
                        Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        }}
                    };

                    await service.PostItens(item);

                    item = new Itens
                    {
                        Id = 9859663,
                        Item = "D320006C8",
                        Referencia = "upload/correcao_9859663.png",
                        Sequencial = "8300003130128",
                        Solicitacao = "2000000886",
                        Situacao = "DISPONIVEL",
                        Ordem = 2,
                        Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 187,
                            Titulo = "Chave de correção 2",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Lido"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Não lido"
                                }
                            }
                        }
                    }
                    };

                    await service.PostItens(item);
                }

                //Forçando reserva do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>()
                        {
                            new Itens_Chaves
                            {
                                ChavesId = 186,
                                Valor = "0"
                            }
                        }
                    };

                    var retorno = await service.PostCorrecoes_Reservada(9859662, correcao);
                }

                //Verificando itens reservados
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new CorrecoesController(context);

                    var retorno = await service.GetCorrecoes_Reservadas();

                    Assert.Single(((List<Itens>)retorno.Value.Itens));
                    Assert.Equal(9859662, ((List<Itens>)retorno.Value.Itens)[0].Id);
                }

                //Forçando reserva do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>()
                        {
                            new Itens_Chaves
                            {
                                ChavesId = 186,
                                Valor = "0"
                            }
                        }
                    };

                    var retorno = await service.PostCorrecoes_Reservada(9859663, correcao);
                }

                //Verificando item está reservado
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new CorrecoesController(context);

                    var retorno = await service.GetCorrecoes_Reservadas();

                    Assert.Equal(2, ((List<Itens>)retorno.Value.Itens).Count);
                    Assert.Equal(9859663, ((List<Itens>)retorno.Value.Itens)[1].Id);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Itens))]
        public async Task Post(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);
                }

                //Forçando coreção do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes(itens.Id, correcao);

                    Assert.IsType<Correcoes>(retorno.Value);
                    Assert.Equal("SUCESSO", retorno.Value.Situacao);
                }

                //Verificando item está corrigido
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Assert.Equal(1, await context.Itens.CountAsync(c=> c.Situacao == "CORRIGIDA"));
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Itens))]
        public async Task Post_Erro_Chave_Incorreta(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);
                }

                //Forçando coreção do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "10"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes(itens.Id, correcao);

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("ERRO", retorno.Value.Situacao);
                    Assert.Equal("CHAVE_INCORRETA", retorno.Value.Tipo);
                }

                //Verificando item está disponível
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Assert.Equal(1, await context.Itens.CountAsync(c => c.Situacao == "DISPONIVEL"));
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Itens))]
        public async Task Post_Erro_Item_Corrigido(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);
                }

                //Forçando coreção do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes(itens.Id, correcao);
                }

                //Tentativa de nova coreção
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes(itens.Id, correcao);

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("ERRO", retorno.Value.Situacao);
                    Assert.Equal("ITEM_CORRIGIDO", retorno.Value.Tipo);
                }

            }
            finally
            {
                connection.Close();
            }
        }

        [Fact]
        public async Task Post_Erro_Item_Invalido()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    var item = new Itens
                    {
                        Id = 9859662,
                        Item = "D020006H6",
                        Referencia = "upload/correcao_9859662.png",
                        Sequencial = "8300003130128",
                        Solicitacao = "2000000885",
                        Situacao = "DISPONIVEL",
                        Ordem = 1,
                        Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        }}
                    };

                    await service.PostItens(item);

                    item = new Itens
                    {
                        Id = 9859663,
                        Item = "D320006C8",
                        Referencia = "upload/correcao_9859663.png",
                        Sequencial = "8300003130128",
                        Solicitacao = "2000000886",
                        Situacao = "DISPONIVEL",
                        Ordem = 2,
                        Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 187,
                            Titulo = "Chave de correção 2",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Lido"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Não lido"
                                }
                            }
                        }
                    }
                    };

                    await service.PostItens(item);

                }

                //Tentando coreção fora de ordem do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes((await context.Itens.FirstAsync(c=> c.Ordem == 2)).Id, correcao);

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("ERRO", retorno.Value.Situacao);
                    Assert.Equal("ITEM_INVALIDO", retorno.Value.Tipo);

                }

            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Itens))]
        public async Task Post_Reservada(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);
                }

                //Forçando reserva do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes_Reservada(itens.Id, correcao);

                    Assert.IsType<Correcoes>(retorno.Value);
                    Assert.Equal("SUCESSO", retorno.Value.Situacao);
                }

                //Verificando item está reservado
                using (var context = new ApplicationDbContext(options))
                {

                    Assert.Equal(1, await context.Itens.CountAsync(c => c.Situacao == "RESERVADA"));
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Itens))]
        public async Task Post_Reservada_Erro_Chave_Incorreta(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);
                }

                //Forçando coreção do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "10"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes_Reservada(itens.Id, correcao);

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("ERRO", retorno.Value.Situacao);
                    Assert.Equal("CHAVE_INCORRETA", retorno.Value.Tipo);
                }

                //Verificando item está disponível
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Assert.Equal(1, await context.Itens.CountAsync(c => c.Situacao == "DISPONIVEL"));
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Itens))]
        public async Task Post_Reservada_Erro_Item_Corrigido(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);
                }

                //Forçando coreção do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes(itens.Id, correcao);
                }

                //Tentativa de nova coreção
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes_Reservada(itens.Id, correcao);

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("ERRO", retorno.Value.Situacao);
                    Assert.Equal("ITEM_CORRIGIDO", retorno.Value.Tipo);
                }

            }
            finally
            {
                connection.Close();
            }
        }

        [Fact]
        public async Task Post_Reservada_Erro_Item_Invalido()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                //Adicionando item 
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    var item = new Itens
                    {
                        Id = 9859662,
                        Item = "D020006H6",
                        Referencia = "upload/correcao_9859662.png",
                        Sequencial = "8300003130128",
                        Solicitacao = "2000000885",
                        Situacao = "DISPONIVEL",
                        Ordem = 1,
                        Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        }}
                    };

                    await service.PostItens(item);

                    item = new Itens
                    {
                        Id = 9859663,
                        Item = "D320006C8",
                        Referencia = "upload/correcao_9859663.png",
                        Sequencial = "8300003130128",
                        Solicitacao = "2000000886",
                        Situacao = "DISPONIVEL",
                        Ordem = 2,
                        Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 187,
                            Titulo = "Chave de correção 2",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Lido"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Não lido"
                                }
                            }
                        }
                    }
                    };

                    await service.PostItens(item);

                }

                //Tentando coreção fora de ordem do item
                using (var context = new ApplicationDbContext(options))
                {

                    var service = new CorrecoesController(context);

                    Correcoes correcao = new Correcoes
                    {
                        Itens_Chaves = new List<Itens_Chaves>(){
                        new Itens_Chaves
                        {
                            ChavesId = 186,
                            Valor = "0"
                        }
                        }
                    };

                    var retorno = await service.PostCorrecoes_Reservada((await context.Itens.FirstAsync(c => c.Ordem == 2)).Id, correcao);

                    Assert.IsType<Correcoes>(retorno.Value);

                    Assert.Equal("ERRO", retorno.Value.Situacao);
                    Assert.Equal("ITEM_INVALIDO", retorno.Value.Tipo);

                }

            }
            finally
            {
                connection.Close();
            }
        }

        private async Task Cadastrar_Itens()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

            using (var context = new ApplicationDbContext(options))
            {
                context.Database.EnsureCreated();
            }

            using (var context = new ApplicationDbContext(options))
            {
                var service = new ItensController(context);

                var item = new Itens
                {
                    Id = 9859662,
                    Item = "D020006H6",
                    Referencia = "upload/correcao_9859662.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000885",
                    Situacao = "DISPONIVEL",
                    Ordem = 1,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        }}
                };

                await service.PostItens(item);

                item = new Itens
                {
                    Id = 9859663,
                    Item = "D320006C8",
                    Referencia = "upload/correcao_9859663.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000886",
                    Situacao = "DISPONIVEL",
                    Ordem = 2,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 187,
                            Titulo = "Chave de correção 2",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Lido"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Não lido"
                                }
                            }
                        }
                    }
                };

                await service.PostItens(item);

                item = new Itens
                {
                    Id = 9859664,
                    Item = "D6530006C7",
                    Referencia = "upload/correcao_9859664.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000886",
                    Situacao = "DISPONIVEL",
                    Ordem = 3,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 188,
                            Titulo = "Chave de correção 3",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "0"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "1"
                                },
                                new Opcoes
                                {
                                    Valor = "2",
                                    Descricao = "2"
                                },
                                new Opcoes
                                {
                                    Valor = "3",
                                    Descricao = "3"
                                },
                                new Opcoes
                                {
                                    Valor = "4",
                                    Descricao = "4"
                                },
                                new Opcoes
                                {
                                    Valor = "5",
                                    Descricao = "5"
                                }
                            }
                        }
                    }
                };

                await service.PostItens(item);
            }
        }
    }
}