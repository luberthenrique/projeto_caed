﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using projeto_caed.Api.Controllers;
using Projeto_Caed.Api.Data;
using projeto_caed.Api.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace projeto_caed.Testes.API
{
    public class ItensApiTest
    {
        public static IEnumerable<object[]> Data =>
        new List<object[]>
        {
            new object[] 
            { 
                new Itens
                {
                    Id = 9859662,
                    Item = "D020006H6",
                    Referencia = "upload/correcao_9859662.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000885",
                    Situacao = "DISPONIVEL",
                    Ordem = 1,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        }}
                } },
            new object[]
            {
                new Itens
                {
                    Id = 9859663,
                    Item = "D320006C8",
                    Referencia = "upload/correcao_9859663.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000886",
                    Situacao = "DISPONIVEL",
                    Ordem = 2,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 187,
                            Titulo = "Chave de correção 2",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Lido"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Não lido"
                                }
                            }
                        }
                    }
                }
            },
            new object[]
            {
                new Itens
                {
                    Id = 9859664,
                    Item = "D6530006C7",
                    Referencia = "upload/correcao_9859664.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000886",
                    Situacao = "DISPONIVEL",
                    Ordem = 3,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        },
                        new Chaves
                        {
                            Id = 188,
                            Titulo = "Chave de correção 3",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "0"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "1"
                                },
                                new Opcoes
                                {
                                    Valor = "2",
                                    Descricao = "2"
                                },
                                new Opcoes
                                {
                                    Valor = "3",
                                    Descricao = "3"
                                },
                                new Opcoes
                                {
                                    Valor = "4",
                                    Descricao = "4"
                                },
                                new Opcoes
                                {
                                    Valor = "5",
                                    Descricao = "5"
                                }
                            }
                        }
                    }
                }
            }
        };

        [Fact]
        public async Task Get()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                context.Database.EnsureCreated();
            }

            using (var context = new ApplicationDbContext(options))
            {
                var service = new ItensController(context);

                var item = new Itens
                {
                    Id = 9859662,
                    Item = "D020006H6",
                    Referencia = "upload/correcao_9859662.png",
                    Sequencial = "8300003130128",
                    Solicitacao = "2000000885",
                    Situacao = "DISPONIVEL",
                    Ordem = 1,
                    Chaves = new List<Chaves>
                    {
                        new Chaves
                        {
                            Id = 186,
                            Titulo = "Chave de correção 1",
                            Opcoes = new List<Opcoes>
                            {
                                new Opcoes
                                {
                                    Valor = "0",
                                    Descricao = "Certo"
                                },
                                new Opcoes
                                {
                                    Valor = "1",
                                    Descricao = "Errado"
                                }
                            }
                        }}
                };

                await service.PostItens(item);

                var retorno = await service.GetItens();

                Assert.IsType<List<Itens>>(retorno.Value);
            }

            using (var context = new ApplicationDbContext(options))
            {
                Assert.Equal(1, await context.Itens.CountAsync());
                Assert.Equal("D020006H6", (await context.Itens.SingleAsync()).Item);
            }
        }

        [Fact]
        public async Task Get_NenhumRetorno()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                context.Database.EnsureCreated();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var service = new ItensController(context);

                var retorno = await service.GetItens();

                Assert.IsType<Retorno>(retorno.Value);
                Assert.Equal("ERRO", ((Retorno)retorno.Value).Situacao);
                Assert.Equal("SEM_ITENS", ((Retorno)retorno.Value).Tipo);
            }
            using (var context = new ApplicationDbContext(options))
            {
                Assert.Equal(0, await context.Itens.CountAsync());

            }
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task Get_Id(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;
                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    await service.PostItens(itens);

                    var retorno = await service.GetItens(itens.Id);

                    Assert.IsType<Itens>(retorno.Value);
                    Assert.Equal(itens.Item, retorno.Value.Item);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task Post(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;
                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    var retorno = await service.PostItens(itens);

                    Assert.IsType<Retorno>(retorno.Value);
                }
                using (var context = new ApplicationDbContext(options))
                {
                    Assert.Equal(1, await context.Itens.CountAsync());
                    Assert.Equal(itens.Item, (await context.Itens.SingleAsync()).Item);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task Put(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;
                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);

                    itens.Situacao = "TESTE";

                    var retorno = await service.PostItens(itens);
                    Assert.IsType<Retorno>(retorno.Value);

                    Assert.Equal("SUCESSO", retorno.Value.Situacao);
                }
                using (var context = new ApplicationDbContext(options))
                {
                    Assert.Equal("TESTE", (await context.Itens.FindAsync(itens.Id)).Situacao);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task Delete(Itens itens)
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;
                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                }

                using (var context = new ApplicationDbContext(options))
                {
                    var service = new ItensController(context);
                    await service.PostItens(itens);
                    var retorno = await service.DeleteItens(itens.Id);

                    Assert.IsType<Retorno>(retorno.Value);
                }
                using (var context = new ApplicationDbContext(options))
                {
                    Assert.Equal(0, await context.Itens.CountAsync());
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
