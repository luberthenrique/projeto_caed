using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using projeto_caed.Api;

namespace projeto_caed.Testes
{
    public class TestClientProvider
    {
        public HttpClient client {get; private set;}
        public TestClientProvider()
        {
            var server = new TestServer(new WebHostBuilder().UseStartup<Startup>());

            client = server.CreateClient();
        }
    }
}